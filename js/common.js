function getNameIputFile (str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }						
    var filename = str.slice(i);			
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}
$(".content-faq .question-block .answer-title a").click(function(e){
    e.preventDefault();
    $(this).closest(".question-block").find(".answer-text").toggle();
});
$(".form-group .radio-block .radio-btn").click(function(e){
    e.preventDefault();
    var parent = $(this).closest(".form-group");
    parent.find(".radio-block.checked input[type=radio]").prop('checked', false);
    parent.find(".radio-block.checked").removeClass("checked");
    $(this).closest(".radio-block").find("input[type=radio]").prop('checked', true);
    $(this).closest(".radio-block").addClass("checked");
});
$(".form-group .checkbox-block .checkbox-btn").click(function(e){
    e.preventDefault();
    var input = $(this).closest(".checkbox-block").find("input[type=checkbox]");
    if(input.is(':checked') === false) {
        input.prop('checked', true);
        $(this).closest(".checkbox-block").addClass("checked");
    }else{
        input.prop('checked', false);
        $(this).closest(".checkbox-block").removeClass("checked");
    }
});

$("#tabs").tabs();
$( ".datepicker" ).datepicker({});